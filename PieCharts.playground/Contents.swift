//: Pieish charts with Core Graphics

import UIKit

struct PieSlice {
    let fromAngle: CGFloat
    let toAngle: CGFloat
}

func pieSlicesForValues(values: [Double]) -> [PieSlice] {
    let accumulatedValues = values.reduce([]) { (sums, element) in
        return sums + [(sums.last ?? 0) + element]
    }
    guard let valueSum = accumulatedValues.last where valueSum != 0 else { return [] }
    let angles = accumulatedValues.map { CGFloat($0/valueSum * M_PI*2) }
    return zip([0] + angles, angles).map { PieSlice(fromAngle: $0.0, toAngle: $0.1) }
}

func pieSlicesForValues2(values: [Double]) -> [PieSlice] {
    var valueSum = Double(0)
    for value in values { valueSum += value }
    guard valueSum != 0 else { return [] }
    var slices = [PieSlice]()
    var lastAngle = CGFloat(0)
    for value in values {
        let toAngle = CGFloat(value/valueSum * M_PI*2) + lastAngle
        slices.append(PieSlice(fromAngle: lastAngle, toAngle: toAngle))
        lastAngle = toAngle
    }
    return slices
}

let values2: [Double] = [1, 2, 3, 2, 1]
pieSlicesForValues(values2).map { $0.fromAngle }
pieSlicesForValues2(values2).map { $0.fromAngle }

func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}

extension CGRect {
    var center: CGPoint {
        return CGPoint(x: origin.x + size.width/2, y: origin.y + size.height/2)
    }
}

func pointOnCircle(center center: CGPoint, radius: CGFloat, angle: CGFloat) -> CGPoint {
    return center + CGPoint(x: radius * cos(angle), y: radius * sin(angle))
}

func pathForPieSlice(slice: PieSlice, bounds: CGRect, donutRatio: CGFloat) -> UIBezierPath {
    let path = UIBezierPath()
    let center = bounds.center
    let radius = bounds.size.width/2
    if donutRatio > 0 && donutRatio < 1 {
        let innerRadius = radius*donutRatio
        let point1 = pointOnCircle(center: center, radius: innerRadius, angle: slice.fromAngle)
        let point2 = pointOnCircle(center: center, radius: innerRadius, angle: slice.toAngle)
        path.moveToPoint(point1)
        path.addArcWithCenter(center, radius: radius, startAngle: slice.fromAngle, endAngle: slice.toAngle, clockwise: true)
        path.addLineToPoint(point2)
        path.addArcWithCenter(center, radius: innerRadius, startAngle: slice.toAngle, endAngle: slice.fromAngle, clockwise: false)
    } else {
        let point = pointOnCircle(center: center, radius: radius, angle: slice.fromAngle)
        path.moveToPoint(center)
        path.addLineToPoint(point)
        path.addArcWithCenter(center, radius: radius, startAngle: slice.fromAngle, endAngle: slice.toAngle, clockwise: true)
    }
    path.closePath()
    return path
}

func pieChartWithValues(values: [Double], dimension: CGFloat, donutRatio: CGFloat = 0, stroke: Bool = false, colors: [UIColor]? = nil) -> UIImage {
    let size = CGSize(width: dimension, height: dimension)
    let inset: CGFloat = 10
    let bounds = CGRect(origin: CGPointZero, size: size).insetBy(dx: inset, dy: inset)
    UIGraphicsBeginImageContext(size)
    defer { UIGraphicsEndImageContext() }
    let context = UIGraphicsGetCurrentContext()
    let N = values.count
    let colors = colors ?? (0..<N).map {
        return UIColor(hue: CGFloat($0) / CGFloat(5*N), saturation: 1, brightness: 1, alpha: 1)
    }
    for (i, slice) in zip(0..<N, pieSlicesForValues(values)) {
        let color = colors[i % colors.count]
        let path = pathForPieSlice(slice, bounds: bounds, donutRatio: donutRatio)
        CGContextSetFillColorWithColor(context, color.CGColor)
        path.fill()
        if stroke {
            path.lineWidth = 2
            CGContextSetStrokeColorWithColor(context, UIColor.blackColor().CGColor)
            path.stroke()
        }
    }
    return UIGraphicsGetImageFromCurrentImageContext()!
}

let values: [Double] = [1, 2, 3, 4, 5, 6]
pieChartWithValues(values, dimension: 400)
pieChartWithValues(values, dimension: 500, donutRatio: 0.5)

